import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { MatDialog, MatTableDataSource, MatSort, MatDialogConfig, MAT_DIALOG_DATA } from '@angular/material';
import { FormBuilder, FormGroup, Validators, NgForm, NgModel, FormControl } from '@angular/forms';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { Observable } from 'rxjs/Observable';
import { TitleService } from '../service/title.service';
import { TeacherService, teacher_reg, date_reg, seatNumber, teacher_data } from '../service/teacher.service';
import { AdminService } from 'src/app/service/admin.service';
import { AngularFireList, AngularFireDatabase, AngularFireObject } from 'angularfire2/database';

import { Books } from '../Student/shared/books';
import { SharedService } from '../Student/shared/shared.service';


import { AngularCsv } from 'angular7-csv/dist/Angular-csv'
import { element } from 'protractor';

// export interface RegistedStd{
//   receive: number;
//   registed: number;
//   surplus: number;
// }

// const ELEMENT_DATA: RegistedStd[] = [
//   {receive: 20,registed:17,surplus:3}
// ];

@Component({
  selector: 'app-teacher',
  templateUrl: './teacher.component.html',
  styleUrls: ['./teacher.component.css']
})
export class TeacherComponent implements OnInit {

  constructor(
    private router: Router,
    public dialog: MatDialog,
    public teacherService: TeacherService,
    public adminService: AdminService,
    private sharedService: SharedService,
    private actRoute: ActivatedRoute,
    private _formBuilder: FormBuilder,
    private firebase: AngularFireDatabase
  ) { }

  teacherList: MatTableDataSource<any>;
  teacherDataColumns: string[] = ['delete', 'id', 'fullName']
  teacherRegColumns: string[] = ['delete', 'id', 'fullName', 'seat', 'registed', 'surplus', 'report'];
  timeColumns: string[] = ['setting', 'dateReg'];

  isHandset$: Observable<boolean>

  teacherRegList: teacher_reg[];
  dateRegList: date_reg[];
  // seatNumberList: seat_number[];
  teacherDataList: teacher_data[]
  seatNumberList: seatNumber[]

  book: Books;
  seatNumber: seatNumber;
  teacherReg: teacher_reg;
  teacherData: teacher_data;

  getStudent: Observable<any>
  getDateReg: Observable<any>;
  getTeacherData: Observable<any>;
  getTeacherReg: Observable<any>;
  getTeacherReg2: Observable<any>
  fullNameTch: string




  ngOnInit() {
    this.shownDetail = false;
    this.getAcademic_years = this.adminService.getAcdemic_years();
    this.getAcademic_years.forEach(data => {
      this.years = data;
    });
    this.teacherService.yearsForm = new FormControl(null, [Validators.required])

    this.seatNumber = new seatNumber();
    this.book = new Books();
    this.teacherReg = new teacher_reg();

    // this.getTeacherData = this.teacherService.getTeacherData2();
    // this.getTeacherReg = this.teacherService.getTeacherReg2();

    // this.teacherService.getTeacherData().subscribe(
    //   list => {
    //     this.teacherDataList = [];
    //     list.forEach(item => {
    //       let a = item.payload.toJSON();
    //       a['$key'] = item.key;
    //       this.teacherDataList.push(a as teacher_data);
    //     })
    //   });

    // this.teacherService.getTeacherReg().subscribe(
    //   list => {
    //     this.teacherRegList = [];
    //     list.forEach(item => {
    //       let a = item.payload.toJSON();
    //       a['$key'] = item.key;
    //       this.teacherRegList.push(a as teacher_reg);
    //     })
    //   });

    // this.teacherService.getDateReg().subscribe(
    //   list => {
    //     this.dateRegList = [];
    //     list.forEach(item => {
    //       let a = item.payload.toJSON();
    //       a['$key'] = item.key;
    //       this.dateRegList.push(a as date_reg);
    //     })
    //   });


    
  }

  settingTimeReg() {
    this.teacherService.initializTimeFormGroup();


    const dialogRef = this.dialog.open(SettingTimeReg);

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }

  editTimeReg(element) {
    this.teacherService.initializTimeFormGroup();
    // this.teacherService.setTimeFormGroup(element);

    this.teacherService.timeFormGroup.setValue({
      $key: element.key, startDate: element.startDate, endDate: element.endDate
    })
    // this.teacherService.yearsForm.setValue(this.teacherService.yearsForm.value)

    console.log(this.teacherService.yearsForm.value, element)

    const dialogRef = this.dialog.open(EditTimeReg);

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }

  openDialog() {
    this.teacherService.initializFormGroup();

    const dialogRef = this.dialog.open(TeacherDialogform);
    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }

  createTeacher() {

    this.teacherService.initializTeacherDataForm();
    const dialogRef = this.dialog.open(TeacherRegData);

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }

  editTeacherData(element) {
    // console.log(element)
    console.log(this.teacherService.yearsForm.value)
    // element.$key = element.teacher_id;
    this.teacherService.initializTeacherDataForm();



    this.teacherService.teacherDataForm.setValue({
      teacher_id: element.teacher_id, NameTitle: element.NameTitle, firstName: element.firstName, lastName: element.lastName
    })
    const dialogRef = this.dialog.open(TeacherUpdateData);

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });

  }

  openDialogTable(element) {

    const dialogRef = this.dialog.open(TeacherStdreceiveDialogtable, {
      data: element.key
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });

  }


  deleteTeacherData(teacher) {
    this.teacherService.deleteTeacherData(teacher.key);
    this.teacherService.success('Delete '+teacher.key+' Successfull !')
    console.log(teacher.key)
  }
  deleteTeacherReg(years, teacher) {
    this.teacherService.deleteTeacherReg(this.teacherService.yearsForm.value, teacher.key);
    this.teacherService.success('Delete ' + teacher.key + ' Successfull !')
    console.log(years, teacher.key)
  }


  getAcademic_years: Observable<any>;
  getSelectYears: Observable<any>;
  yearsData: Observable<any>
  years: Observable<any>;
  // yearsForm: FormControl
  shownButton = false;
  shownDetail = false;
  shown() {
    console.log(this.teacherService.yearsForm.value)
    if (this.teacherService.yearsForm.value == null) {
      this.shownDetail = false;
      console.log("null")
    } else {
      this.shownDetail = true;
      this.getSelectYears = this.adminService.getSelectYear(this.teacherService.yearsForm.value);
      this.getSelectYears.forEach(data => {
        this.yearsData = data;
        // console.log(data)
      })
      this.getTeacherData = this.teacherService.getTeacherData2();
      this.getTeacherData.forEach(data => {
        console.log('data:', data)
        this.teacherDataList = data;
      })
      this.getDateReg = this.teacherService.getDateReg2(this.teacherService.yearsForm.value);
      this.getDateReg.forEach(data => {
        if (data.length != 0) {
          console.log('date:', data)
          this.shownButton = false;
          this.dateRegList = data;
        } else { this.shownButton = true; }

      })

      this.getTeacherReg = this.teacherService.getTeacherReg3(this.teacherService.yearsForm.value);
      this.getTeacherReg.forEach(data => {
        console.log('Reg:', data)
        this.teacherRegList = data;
      })
    }
  }

  // routing path//
  // TeacherUI() {
  //   this.router.navigate(['teacher'])
  // }

signOut() {
    this.router.navigate(['login'])
  }




}


////////////////////////////////////////////////
@Component({
  selector: 'setting-time-reg',
  templateUrl: './teacher.timereg.html',
})
export class SettingTimeReg {

  constructor(
    private fb: FormBuilder,
    public teacherService: TeacherService,
    public adminService: AdminService
  ) { }
  dateReg: date_reg;
  teacherReg: teacher_reg;
  // timeFormGroup: FormGroup;
  getDateReg: Observable<any>;

  // getAcademic_years: Observable<any>
  // years: Observable<any>
  // yearsForm: FormControl


  ngOnInit() {
    this.dateReg = new date_reg();
    this.teacherReg = new teacher_reg();
    this.getDateReg = this.teacherService.getDateReg();

    // this.getAcademic_years = this.adminService.getAcdemic_years();
    // this.getAcademic_years.forEach(data => {
    //   this.years = data;
    // });
    // this.yearsForm = new FormControl('', [Validators.required])

  }


  onSubmit() {
    if (this.teacherService.timeFormGroup.valid) {
      this.teacherService.insertDateReg(this.teacherService.timeFormGroup.value, this.teacherService.yearsForm.value)
    }
    this.teacherService.success('Submit successfull');
    console.log(this.dateReg.startDate, ' - ', this.dateReg.endDate);
  }


}

///////////////////////////////////////////////////
@Component({
  selector: 'edit-time-reg',
  templateUrl: './teacher.timeedit.html',
})
export class EditTimeReg {

  constructor(
    private fb: FormBuilder,
    public teacherService: TeacherService,
    public adminService: AdminService
  ) { }
  dateReg: date_reg;
  teacherReg: teacher_reg;
  // timeFormGroup: FormGroup;
  getDateReg: Observable<any>;

  // getAcademic_years: Observable<any>
  // years: Observable<any>
  // yearsForm: FormControl


  ngOnInit() {
    this.dateReg = new date_reg();
    this.teacherReg = new teacher_reg();
    this.getDateReg = this.teacherService.getDateReg();

    // this.getAcademic_years = this.adminService.getAcdemic_years();
    // this.getAcademic_years.forEach(data => {
    //   this.years = data;
    // });
    // this.yearsForm = new FormControl('', [Validators.required])

  }


  onSubmit() {
    if (this.teacherService.timeFormGroup.valid) {
      this.teacherService.insertDateReg(this.teacherService.timeFormGroup.value, this.teacherService.yearsForm.value)
    }
    console.log(this.dateReg.startDate, ' - ', this.dateReg.endDate);
  }

  onUpdate() {
    if (this.teacherService.timeFormGroup.valid) {
      this.teacherService.updateTime(this.teacherService.timeFormGroup.value, this.teacherService.yearsForm.value)
    }
    this.teacherService.success('Update successfull')
    console.log(this.dateReg.startDate, ' - ', this.dateReg.endDate);
  }

}


////////////////////////////////////////////////

@Component({
  selector: 'teacher-dialogform',
  templateUrl: './teacher.dialogform.html',
})
export class TeacherDialogform {

  teacherReg: teacher_reg;
  dateReg: date_reg;
  seatNumber: seatNumber;
  teacherData:teacher_data;
  getDateReg: Observable<any>;
  getTeacherData: Observable<any>;
  getTeacherReg: Observable<any>;
  getSeat: Observable<any>;
  teacherDataList: teacher_data[]
  // getAcademic_years: Observable<any>
  // years: Observable<any>
  // yearsForm: FormControl
  obj1: Object;
  check_id = false;
  isEditable = false;
  isLinear = true;
  constructor(
    public teacherService: TeacherService,
    public adminService: AdminService,
    private fb: FormBuilder,
   
  ) { }

  selectFormControl :FormControl;

  ngOnInit() {

    this.check_id = false;
    this.teacherReg = new teacher_reg();
    this.dateReg = new date_reg();
    this.seatNumber = new seatNumber();
    this.teacherData = new teacher_data();
    
    this.getTeacherData = this.teacherService.getTeacherData2();
    this.getTeacherData.forEach(data => {
      this.teacherDataList = data;
    })
    this.selectFormControl = new FormControl(null, [Validators.required]);

  }


  check_tchId() {
    let i = 0;
    this.getTeacherData = this.teacherService.getTeacherData2()
    this.getTeacherData.forEach(data => {
      for (i; i < data.length; i++) {
        if (this.teacherReg.teacher_id == data[i].teacher_id) {
          this.validate_id(this.teacherReg.teacher_id, data[i].teacher_id);
          this.teacherReg.fullName = data[i].NameTitle + data[i].firstName + ' ' + data[i].lastName;
          // this.teacherReg.dateReg = this.dateReg.startDate;

          this.check_id = true;
          this.obj1 = data[i];


          console.log(this.teacherReg.fullName);
          console.log("data", data[i]);
          break;

        } else {
          this.validate_id(this.teacherReg.teacher_id, data[i].teacher_id);
          this.check_id = false;
          console.log("No data");
        }
      }
    });

  }



  validate_id(input_id, teacher_id_data) {
    this.teacherService.firstFormGroup = this.fb.group({
      teacher_id: [input_id, [Validators.required, Validators.pattern(teacher_id_data)]]
    });
  }



  onSubmit() {
    console.log(this.teacherReg.fullName);
    console.log(this.teacherReg.seat);
    this.teacherReg.count = this.teacherReg.seat
    if (this.teacherService.firstFormGroup.valid && this.teacherService.secondFormGroup.valid) {
      this.teacherService.insertTeacherReg(this.teacherService.firstFormGroup.value
        , this.teacherService.secondFormGroup.value, this.teacherReg, this.teacherService.yearsForm.value);

      // let i=1;
      // for(i; i<=this.teacherReg.seat; i++){
      //   if(i<=this.teacherReg.seat){
      //     this.teacherService.insertSeatNumber(this.seatNumber,this.teacherReg.teacher_id,i)
      //     console.log(i)
      //   }else{
      //     console.log("break") 
      //     break;
      //   }
      // }


      this.teacherService.firstFormGroup.reset;
      this.teacherService.secondFormGroup.reset;
      this.teacherService.success('Submitted successfully !');
    }
  }


  onClear() {
    // this.teacherService.form.reset();
    // this.teacherService.form2.reset();
    // this.teacherService.initializaFormGroup();
  }
}

///////////////////////////////////////////////////////////////

@Component({
  selector: 'teacher-regdata',
  templateUrl: './teacher.regdata.html',
})
export class TeacherRegData {

  dialogForm: TeacherDialogform;
  teacherReg: teacher_reg;
  teacherCompo: TeacherComponent

  teacherData: teacher_data;
  getTeacherData: Observable<any>

  // getAcademic_years: Observable<any>
  // years: Observable<any>
  // yearsForm: FormControl

  constructor(
    public teacherService: TeacherService,
    public adminService: AdminService

  ) { }

  ngOnInit() {
    this.getTeacherData = this.teacherService.getTeacherData();
    this.teacherData = new teacher_data();

    // this.getAcademic_years = this.adminService.getAcdemic_years();
    // this.getAcademic_years.forEach(data => {
    //   this.years = data;
    // });
    // this.yearsForm = new FormControl('', [Validators.required])

  }


  onSubmit() {
    // this.teacherService.updateTeacher(this.teacherService.secondFormGroup.value)
    if (this.teacherService.teacherDataForm.valid) {
      this.teacherService.insertTeacherData(this.teacherService.teacherDataForm.value)
      this.teacherService.success('Create ' + this.teacherService.teacherDataForm.value.teacher_id + ' successfully !');
    } else { this.teacherService.warning('Can not submit') }
    console.log(this.teacherService.teacherDataForm.value.teacher_id)
  }


  // updateTeacherData(){
  //   this.teacherService.updateTeacherData(this.teacherService.teacherDataForm.value);
  //   this.teacherService.success('Update successfully')
  // }

}

//////////////////////////////////////////////////////////////

@Component({
  selector: 'teacher-updatedata',
  templateUrl: './teacher.updatedata.html',
})
export class TeacherUpdateData {

  dialogForm: TeacherDialogform;
  teacherReg: teacher_reg;
  teacherCompo: TeacherComponent

  teacherData: teacher_data;
  getTeacherData: Observable<any>

  // getAcademic_years: Observable<any>
  // years: Observable<any>
  // yearsForm: FormControl

  constructor(
    public teacherService: TeacherService,
    public adminService: AdminService
  ) { }

  ngOnInit() {

    this.getTeacherData = this.teacherService.getTeacherData();
    this.teacherData = new teacher_data();

    // this.getAcademic_years = this.adminService.getAcdemic_years();
    // this.getAcademic_years.forEach(data => {
    //   this.years = data;
    // });
    // this.yearsForm = new FormControl('', [Validators.required])

  }


  onSubmit() {
    // this.teacherService.updateTeacher(this.teacherService.secondFormGroup.value)
    if (this.teacherService.teacherDataForm.valid) {
      this.teacherService.insertTeacherData(this.teacherService.teacherDataForm.value)
      this.teacherService.success('Edit ' + this.teacherService.teacherDataForm.value.teacher_id + ' successfully !');
    } else { this.teacherService.warning('Can not submit') }
    console.log(this.teacherService.teacherDataForm.value.teacher_id)
  }


  // updateTeacherData(){
  //   this.teacherService.updateTeacherData(this.teacherService.teacherDataForm.value);
  //   this.teacherService.success('Update successfully')
  // }

}



////////////////////////////////////////////////////////////////////
@Component({
  selector: 'teacher-stdreceive-dialogtable',
  templateUrl: './teacher-stdreceive.dialogtable.html',
})
export class TeacherStdreceiveDialogtable {

  constructor(
    public teacherService: TeacherService,
    public adminService: AdminService,
    private firebase: AngularFireDatabase,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  seatNumberList: any[];
  displayedColumns2: string[] = ['stdId', 'stdName', 'gpa'];
  seatNumber: seatNumber;
  teacherReg: teacher_reg;
  getTeacherReg: Observable<any>
  teacherRegList: teacher_reg[]
  id: string



  ngOnInit() {


    console.log(this.data)
    // console.log(this.teacherService.yearsForm.value)

    this.teacherService.getSeatNumber(this.teacherService.yearsForm.value, this.data).subscribe(
      list => {
        this.seatNumberList = [];
        list.forEach(item => {
          let a = item.payload.toJSON();
          a['$key'] = item.key;
          this.seatNumberList.push(a as seatNumber);
        })


      })
  }

  csvOptions = {
    fieldSeparator: ",",
    quoteStrings: '"',
    decimalseparator: '.',
    showLabels: true,
    showTitle: false,
    title: "",
    useBom: true,
    noDownload: false,
    headers: ["รหัสนักศึกษา", "ชื่อ-นามสกุล", "Gpa"]
  };

  dataCsv: any[] = new Array();

  downloadCsv() {
    //console.log(this.seatNumberList[0].name)
    for (let i = 0; i < this.seatNumberList.length; i++) {
      this.dataCsv.push({
        "std_id": this.seatNumberList[i].std_id,
        "name": this.seatNumberList[i].name,
        "gpa": this.seatNumberList[i].gpa
      });
      //new AngularCsv(this.seatNumberList, 'StudentList', this.csvOptions);
    }
    
    new AngularCsv(this.dataCsv, this.data+'_stdList', this.csvOptions);
    console.log(this.dataCsv);


  }

}