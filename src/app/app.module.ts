import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {DatePipe, registerLocaleData} from '@angular/common';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {
MatAutocompleteModule,
MatBadgeModule,
MatBottomSheetModule,
MatButtonModule,
MatButtonToggleModule,
MatCardModule,
MatCheckboxModule,
MatChipsModule,
MatDatepickerModule,
MatDialogModule,
MatDividerModule,
MatExpansionModule,
MatGridListModule,
MatIconModule,
MatInputModule,
MatListModule,
MatMenuModule,
MatNativeDateModule,
MatPaginatorModule,
MatProgressBarModule,
MatProgressSpinnerModule,
MatRadioModule,
MatRippleModule,
MatSelectModule,
MatSidenavModule,
MatSliderModule,
MatSlideToggleModule,
MatSnackBarModule,
MatSortModule,
MatStepperModule,
MatTableModule,
MatTabsModule,
MatToolbarModule,
MatTooltipModule,
MatTreeModule,
} from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RoutingModule} from 'src/app/routing/routing.module';
import { AngularFireModule} from 'angularfire2';
import { firebaseConfig } from './../environments/firebase.config';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { PapaParseModule } from 'ngx-papaparse';

import { RegisterStudentComponent,RegisterStudentDialog } from './Student/register-student/register-student.component';
import { TeacherComponent,SettingTimeReg,EditTimeReg,TeacherDialogform,TeacherRegData,TeacherUpdateData,TeacherStdreceiveDialogtable} from './teacher/teacher.component';
import {TeacherService} from './service/teacher.service';
import {TitleService} from './service/title.service';
import { AdminComponent,DialogAlert } from './admin/admin.component';
import { from } from 'rxjs';
import { HomepageComponent } from './homepage/homepage.component';
import { DashboardComponent } from './admin/dashboard/dashboard.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NgZorroAntdModule, NZ_I18N, en_US } from 'ng-zorro-antd';
import { HttpClientModule } from '@angular/common/http';
import en from '@angular/common/locales/en';

registerLocaleData(en);

@NgModule({
  declarations: [
    AppComponent,
    AdminComponent,
    DialogAlert,
    HomepageComponent,
    RegisterStudentComponent,
    TeacherComponent,
    SettingTimeReg,
    EditTimeReg,
    TeacherDialogform,
    TeacherRegData,
    TeacherUpdateData,
    TeacherStdreceiveDialogtable,
    DashboardComponent,
    RegisterStudentDialog
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    RoutingModule,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    PapaParseModule,
    BrowserAnimationsModule,
    MatAutocompleteModule,
    MatBadgeModule,
    MatBottomSheetModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatStepperModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatTreeModule,
    FlexLayoutModule,
    NgZorroAntdModule,
    HttpClientModule
  ],
  providers: [TitleService,TeacherService,DatePipe, { provide: NZ_I18N, useValue: en_US }],
  entryComponents:[AdminComponent,
    DialogAlert,
    TeacherComponent,
    SettingTimeReg,
    EditTimeReg,
    TeacherDialogform,
    TeacherRegData,
    TeacherUpdateData,
    RegisterStudentDialog,
     TeacherStdreceiveDialogtable
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
